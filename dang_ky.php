<!DOCTYPE html>
<html lang="en">
    
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Đăng kí</title>

        <!-- Icon css link -->
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <link href="vendors/linearicons/style.css" rel="stylesheet">
        <link href="vendors/flat-icon/flaticon.css" rel="stylesheet">
        <link href="vendors/stroke-icon/style.css" rel="stylesheet">
        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        
        <!-- Rev slider css -->
        <link href="vendors/revolution/css/settings.css" rel="stylesheet">
        <link href="vendors/revolution/css/layers.css" rel="stylesheet">
        <link href="vendors/revolution/css/navigation.css" rel="stylesheet">
        <link href="vendors/animate-css/animate.css" rel="stylesheet">
        
        <!-- Extra plugin css -->
        <link href="vendors/owl-carousel/owl.carousel.min.css" rel="stylesheet">
        <link href="vendors/magnifc-popup/magnific-popup.css" rel="stylesheet">
        
        <link href="css/style.css" rel="stylesheet">
        <link href="css/responsive.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        
        <!--================Main Header Area =================-->
		<header class="main_header_area">
            <div class="top_header_area row m0">
                <div class="container">
                    <div class="float-left">
                        <a href="tell:0123456789"><i class="fa fa-phone" aria-hidden="true"></i> 0123456789</a>
                        <a href="mainto:laylungteam888@gmail.com"><i class="fa fa-envelope-o" aria-hidden="true"></i> laylungteam888@gmail.com</a>
                    </div>
                    
                    <div class="float-right">
                        <ul class="h_social list_style">
                            <li><a href="dang_ky.php">Đăng kí</a></li>
                            <li><a href="dang_nhap.php">Đăng nhập</i></a></li>
                        
                        </ul>
                        <ul class="h_search list_style">
                            <li class="shop_cart"><a href="#"><i class="lnr lnr-cart"></i></a></li>
                            <li><a class="popup-with-zoom-anim" href="#test-search"><i class="fa fa-search"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="main_menu_area">
                <div class="container">
                    <nav class="navbar navbar-expand-lg navbar-light bg-light">
                        
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="my_toggle_menu">
                                <span></span>
                                <span></span>
                                <span></span>
                            </span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav mr-auto">
                                <li class="dropdown submenu active">
                                    <a class="dropdown-toggle"  href="index.php" role="button" >Trang chủ</a>
                                </li>
                                    <li class="dropdown submenu">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Các sản phẩm</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="mau_ve.php">Màu vẽ</a></li>
                                        <li><a href="but_ve.php">Bút vẽ</a></li>
                                        <li><a href="giay_ve.php">Giấy vẽ</a></li>
                                        <li><a href="artbook.php">Artbook</a></li>
                                        <li><a href="washi.php">Washi tape</a></li>
                                        <li><a href="bang_ve.php">Bảng vẽ điển tử</a></li>
                                    </ul>
                                    </li>
                        </div>
                    </nav>
                </div>
            </div>
        </header>
        <!--================End Main Header Area =================-->
        
        <!--================Contact Form Area =================-->
        <section class="contact_form_area p_100">
        	<div class="container">
        		<div class="main_title">
                    <h2>Đăng kí</h2>
                    <h5>Hãy đăng kí nhé</h5>
				</div>
       			<div class="row">
       				<div class="col-lg-7">
                        
                        <div class="noti">
                        <?php
                        if(isset($_GET['error_dang_ky'])){
                            echo "<h1>Đăng ký thất bại</h1>";
                        }
                        else if(isset($_GET['success'])){
                            echo "<h1>Đã đăng xuất</h1>";
                        }
                        else if(isset($_GET['error_session'])){
                            echo "<h1>Bạn phải đăng nhập</h1>";
                        }
                        elseif (isset($_GET['duplicate_email'])) {
                            echo "<h1>Email đã tồn tại</h1>";
                        }
                        ?>
                        </div>
                    
                    </div>
       					<form class="row contact_us_form" action="dang_ky_process.php" method="post">
							<div class="form-group col-md-6">
								<input type="text" class="form-control" id="ten_khach_hang" name="ten_khach_hang" placeholder="Tên của bạn" onchange="Testten()" required >
                                <div id="ten_sai" class="form-group col-md-6" ></div>
							</div>

							<div class="form-group col-md-6">
								<input type="text" class="form-control" id="email_khach_hang" name="email_khach_hang" placeholder="Địa chỉ email" onchange="Testemail()" required>
                                <div id="email_sai" class="form-group col-md-6"></div>
							</div>
                            <div class="form-group col-md-6">
                                <input type="password" class="form-control" id="mat_khau_khach_hang" name="mat_khau_khach_hang" placeholder="Hãy nhập mật khẩu">
                            </div>
							
							<div class="form-group col-md-12">
								<textarea class="form-control" name="dia_chi_khach_hang" id="dia_chi_khach_hang" rows="1" placeholder="Địa chỉ"onchange="Testdiachi()" required></textarea>
                                <div class="form-group col-md-6"id="diachi_sai"></div>
							</div>
                            <div class="form-group col-md-12">
                                <input type="text" class="form-control" id="sdt_khach_hang" name="sdt_khach_hang" placeholder="Số điện thoại">
                            </div>
                           
                            <div class="form-group col-md-6">
                                 <div class="form-group col-md-6">
                                <label class="form-element-label" for="ngay_sinh_khach_hang">Ngày Tháng Năm Sinh</label>
                                </div>
                                <input type="date" class="form-control" id="ngay_sinh_khach_hang" name="ngay_sinh_khach_hang" placeholder="Ngày sinh">
                            </div>
                            <div  class="form-group col-md-12">
                                <div class="form-radio form-radio-inline">
                                <div class="form-radio-legend">Giới tính</div>
                                    <label class="form-radio-label">
                                        <input name=gioi_tinh_khach_hang class="form-radio-field" type="radio" required checked value="0" />
                                        <i class="form-radio-button"></i>
                                        <span>Nam</span>
                                    </label>
                                    <label class="form-radio-label">
                                        <input name=gioi_tinh_khach_hang class="form-radio-field" type="radio" required value="1" />
                                        <i class="form-radio-button"></i>
                                        <span>Nữ</span>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <button name="dang_ky" class="btn order_s_btn form-control" onclick="return dang_ky()">Đăng kí ngay</button>
                            </div>
						</form>
                      <!--  <script type="text/javascript">
                            function Testten(){
                                var ten_khach_hang = document.getElementById('ten_khach_hang').value;
                                var regex_ten = /^[a-zàáâãèéêếìíòóôõùúăđĩũơưăạảấầẩẫậắằẳẵặẹẻẽềềểễệỉịọỏốồổỗộớờởỡợụủứừửữựỳỵỷỹ\s]{5,50}$/i;
                                var result_ten = regex_ten.test(ten_khach_hang);
                                    if(result_ten==false){
                                        document.getElementById('ten_sai').innerHTML = "Tên không hợp lệ. Nhập lại!";
                                    }
                            }
                            function Testdiachi(){
                                var dia_chi_khach_hang = document.getElementById('dia_chi_khach_hang').value;
                                var regex_diachi = /^[a-zàáâãèéêếìíòóôõùúăđĩũơưăạảấầẩẫậắằẳẵặẹẻẽềềểễệỉịọỏốồổỗộớờởỡợụủứừửữựỳỵỷỹ\s]{5,100}$/i;
                                var result_diachi = regex_diachi.test(dia_chi_khach_hang);
                                    if (result_diachi==false){
                                        document.getElementById('diachi_sai').innerHTML = "Địa chỉ có kí tự không đúng";
                                    }
                            }
                            function Testemail(){
                                var email_khach_hang = document.getElementById('email_khach_hang')
                                var regex_email= /^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/;
                                var result_email = regex_email.test(email_khach_hang);
                                    if (result_email==false) {
                                        document.getElementById('email_sai').innerHTML = "Sai cú pháp email rồi!"; 
                                    }
                            }

                        </script> --> 
       				
                </div>
            </div>
        </section>
        <!--================End Contact Form Area =================-->
        
        <!--================End Banner Area =================-->
       
        <!--================End Banner Area =================-->
        
        <!--================Newsletter Area =================-->
        
        <!--================End Newsletter Area =================-->
        
        <!--================Footer Area =================-->
        <footer class="footer_area">
            <div class="footer_widgets">
                <div class="container">
                    <div class="row footer_wd_inner">
                        <div class="col-lg-3 col-6">
                            <aside class="f_widget f_about_widget">
                                <img src="img/logo_cuoi.jpg" alt="">
                                <p>Đây là danh sách liên lạc và điều cần thiết.</p>
                                <ul class="nav">
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                </ul>
                            </aside>
                        </div>
                        <div class="col-lg-3 col-6">
                            <aside class="f_widget f_link_widget">
                                <div class="f_title">
                                    <h3>Nhanh</h3>
                                </div>
                                <ul class="list_style">
                                    <li><a href="trang_ca_nhan.php">Tài khoản của bạn</a></li>
                                    <li><a href="xem_hoa_don.php">Hóa đơn</a></li>
                                </ul>
                            </aside>
                        </div>
                        <div class="col-lg-3 col-6">
                            <aside class="f_widget f_link_widget">
                                <div class="f_title">
                                    <h3>Thời gian làm việc</h3>
                                </div>
                                <ul class="list_style">
                                    <li>Thứ Hai. :  Thứ Sáu.: 8 am - 8 pm</li>
                                    <li>Thứ Bảy. : 9am - 4pm</li>
                                    <li>Chủ Nhật. : Closed</li>
                                </ul>
                            </aside>
                        </div>
                        <div class="col-lg-3 col-6">
                            <aside class="f_widget f_contact_widget">
                                <div class="f_title">
                                    <h3>Thông tin liên lạc</h3>
                                </div>
                                <h4>0123456789</h4>
                                <p>Nhà Lẫy Lừng <br />Online</p>
                                <h5>laylungteam888@gmail.com</h5>
                            </aside>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!--================End Footer Area =================-->
        
        <!--================Search Box Area =================-->
        <div class="search_area zoom-anim-dialog mfp-hide" id="test-search">
            <div class="search_box_inner">
                <h3>Tìm kiếm</h3>
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Tìm kiếm...">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button"><i class="icon icon-Search"></i></button>
                    </span>
                </div>
            </div>
        </div>
        <!--================End Footer Area =================-->
        
        
        <!--================Search Box Area =================-->
        <div class="search_area zoom-anim-dialog mfp-hide" id="test-search">
            <div class="search_box_inner">
                <h3>Search</h3>
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button"><i class="icon icon-Search"></i></button>
                    </span>
                </div>
            </div>
        </div>
        <!--================End Search Box Area =================-->
        
        <!--================Contact Success and Error message Area =================-->
        
        
        
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="js/jquery-3.2.1.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <!-- Rev slider js -->
        <script src="vendors/revolution/js/jquery.themepunch.tools.min.js"></script>
        <script src="vendors/revolution/js/jquery.themepunch.revolution.min.js"></script>
        <script src="vendors/revolution/js/extensions/revolution.extension.actions.min.js"></script>
        <script src="vendors/revolution/js/extensions/revolution.extension.video.min.js"></script>
        <script src="vendors/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
        <script src="vendors/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
        <script src="vendors/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
        <!-- Extra plugin js -->
        <script src="vendors/owl-carousel/owl.carousel.min.js"></script>
        <script src="vendors/magnifc-popup/jquery.magnific-popup.min.js"></script>
        <script src="vendors/datetime-picker/js/moment.min.js"></script>
        <script src="vendors/datetime-picker/js/bootstrap-datetimepicker.min.js"></script>
        <script src="vendors/nice-select/js/jquery.nice-select.min.js"></script>
        <script src="vendors/jquery-ui/jquery-ui.min.js"></script>
        <script src="vendors/lightbox/simpleLightbox.min.js"></script>
        <!--gmaps Js-->
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjCGmQ0Uq4exrzdcL6rvxywDDOvfAu6eE"></script>
        <script src="js/gmaps.min.js"></script>
        <script src="js/map-active.js"></script>
        
        <!-- contact js -->
        <script src="js/jquery.form.js"></script>
        <script src="js/jquery.validate.min.js"></script>
        <script src="js/contact.js"></script>
        
        <script src="js/theme.js"></script>
    </body>

</html>