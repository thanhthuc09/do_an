-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th1 14, 2019 lúc 05:35 AM
-- Phiên bản máy phục vụ: 10.1.34-MariaDB
-- Phiên bản PHP: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `test`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `admin`
--

CREATE TABLE `admin` (
  `ma_admin` int(10) UNSIGNED NOT NULL,
  `ten_admin` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  `email_admin` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  `mat_khau_admin` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  `dia_chi_admin` varchar(200) CHARACTER SET utf8mb4 NOT NULL,
  `sdt_admin` char(13) CHARACTER SET utf8mb4 NOT NULL,
  `ngay_sinh_admin` date NOT NULL,
  `gioi_tinh_admin` tinyint(1) NOT NULL,
  `cap_do` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `admin`
--

INSERT INTO `admin` (`ma_admin`, `ten_admin`, `email_admin`, `mat_khau_admin`, `dia_chi_admin`, `sdt_admin`, `ngay_sinh_admin`, `gioi_tinh_admin`, `cap_do`) VALUES
(2, 'nguyen huong', 'huong@gmail.com', '357', 'ha noi', '0965779640', '1999-12-25', 0, 0),
(3, 'lien', 'abc@gmail.com', '123', 'ha nam', '0978523543', '2000-05-11', 0, 0),
(12, 'thu', 'thu@gmail.com', '123', 'ha noi', '0969827287', '1999-05-15', 0, 1),
(14, 'quy', 'abbc@gmail.com', '123', 'ha nam', '0258741369', '2001-10-05', 1, 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `danh_muc`
--

CREATE TABLE `danh_muc` (
  `ma_danh_muc` int(10) UNSIGNED NOT NULL,
  `ten_danh_muc` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `danh_muc`
--

INSERT INTO `danh_muc` (`ma_danh_muc`, `ten_danh_muc`) VALUES
(7, 'Art book'),
(4, 'Bảng vẽ điện tử'),
(2, 'Bút vẽ'),
(6, 'Combo'),
(3, 'Giấy'),
(1, 'Màu vẽ'),
(5, 'Nhóm dụng cụ'),
(9, 'Sản phẩm khác'),
(8, 'Washi tape');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `danh_muc_san_pham`
--

CREATE TABLE `danh_muc_san_pham` (
  `ma_san_pham` int(10) UNSIGNED NOT NULL,
  `ma_danh_muc` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `danh_muc_san_pham`
--

INSERT INTO `danh_muc_san_pham` (`ma_san_pham`, `ma_danh_muc`) VALUES
(4, 1),
(5, 2),
(6, 2),
(7, 2),
(8, 2),
(10, 3),
(12, 4),
(13, 2),
(20, 7);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `hoa_don`
--

CREATE TABLE `hoa_don` (
  `ma_hoa_don` int(10) UNSIGNED NOT NULL,
  `ma_khach_hang` int(10) UNSIGNED NOT NULL,
  `thoi_gian_dat_hang` datetime NOT NULL,
  `gia` int(10) UNSIGNED NOT NULL,
  `ten_khach_hang` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  `dia_chi_khach_hang` text CHARACTER SET utf8mb4 NOT NULL,
  `sdt_khach_hang` char(13) CHARACTER SET utf8mb4 NOT NULL,
  `tinh_trang` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `hoa_don`
--

INSERT INTO `hoa_don` (`ma_hoa_don`, `ma_khach_hang`, `thoi_gian_dat_hang`, `gia`, `ten_khach_hang`, `dia_chi_khach_hang`, `sdt_khach_hang`, `tinh_trang`) VALUES
(1, 3, '0000-00-00 00:00:00', 142, 'bkacad', 'hanoi', 'sdt_khach_han', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `hoa_don_chi_tiet`
--

CREATE TABLE `hoa_don_chi_tiet` (
  `so_luong` int(10) UNSIGNED NOT NULL,
  `ma_hoa_don` int(10) UNSIGNED NOT NULL,
  `ma_san_pham` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `hoa_don_chi_tiet`
--

INSERT INTO `hoa_don_chi_tiet` (`so_luong`, `ma_hoa_don`, `ma_san_pham`) VALUES
(1, 1, 4),
(1, 1, 5);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `khach_hang`
--

CREATE TABLE `khach_hang` (
  `ma_khach_hang` int(10) UNSIGNED NOT NULL,
  `ten_khach_hang` varchar(50) NOT NULL,
  `email_khach_hang` varchar(50) NOT NULL,
  `mat_khau_khach_hang` varchar(50) NOT NULL,
  `dia_chi_khach_hang` varchar(200) NOT NULL,
  `sdt_khach_hang` char(13) NOT NULL,
  `ngay_sinh_khach_hang` date NOT NULL,
  `gioi_tinh_khach_hang` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `khach_hang`
--

INSERT INTO `khach_hang` (`ma_khach_hang`, `ten_khach_hang`, `email_khach_hang`, `mat_khau_khach_hang`, `dia_chi_khach_hang`, `sdt_khach_hang`, `ngay_sinh_khach_hang`, `gioi_tinh_khach_hang`) VALUES
(1, 'Tham', 'tham@gmail.com', '123', 'hanoi', '0969827287', '2000-05-08', 1),
(2, 'Quang', 'quang@gmail.com', '1234', 'thaibinh', '0969287827', '2000-09-03', 0),
(3, 'bkacad', 'bkacad@gmail.com', '123', 'hanoi', '096812515', '2019-01-08', 0),
(4, 'khoa', 'a@gmail.com', '123', 'hanoi', '123456789', '1999-01-11', 0),
(5, 'hieu', 'b@gmail.com', '123', 'ha noi', '147852369', '2000-12-07', 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `loai_san_pham`
--

CREATE TABLE `loai_san_pham` (
  `ma_loai_san_pham` int(10) UNSIGNED NOT NULL,
  `ten_loai_san_pham` varchar(100) CHARACTER SET utf8mb4 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `loai_san_pham`
--

INSERT INTO `loai_san_pham` (`ma_loai_san_pham`, `ten_loai_san_pham`) VALUES
(4, 'Acrylic, phấn tiên'),
(16, 'Artbook'),
(12, 'Bảng vẽ điện tử'),
(8, 'Bút chì,than'),
(6, 'Bút cọ màu'),
(5, 'Bút marker'),
(9, 'Bút net,line,brush'),
(7, 'Bút sắt,bút mực'),
(10, 'Giấy Canson'),
(3, 'Màu bột'),
(2, 'Màu chì'),
(1, 'Màu nước'),
(13, 'Nhóm dụng cụ'),
(11, 'Sketchbook'),
(14, 'Vẽ truyền thần'),
(15, 'Vẽ truyện tranh'),
(17, 'Washi tape');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `nha_san_xuat`
--

CREATE TABLE `nha_san_xuat` (
  `ma_nha_san_xuat` char(20) CHARACTER SET latin1 NOT NULL,
  `ten_nha_san_xuat` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `nha_san_xuat`
--

INSERT INTO `nha_san_xuat` (`ma_nha_san_xuat`, `ten_nha_san_xuat`) VALUES
('DL', 'Taiwan'),
('N', 'Nga'),
('NB', 'Japan'),
('P', 'Canson Pháp'),
('TH', 'Thượng Hải'),
('TQ', 'Trung Quốc'),
('VN', 'Việt Nam');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `san_pham`
--

CREATE TABLE `san_pham` (
  `ma_san_pham` int(10) UNSIGNED NOT NULL,
  `ten_san_pham` varchar(100) NOT NULL,
  `gia` int(10) UNSIGNED NOT NULL,
  `gia_khuyen_mai` int(10) UNSIGNED DEFAULT NULL,
  `anh` varchar(200) NOT NULL,
  `so_luong` int(10) UNSIGNED NOT NULL,
  `mo_ta` text NOT NULL,
  `ma_nha_san_xuat` char(20) CHARACTER SET latin1 NOT NULL,
  `ma_loai_san_pham` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `san_pham`
--

INSERT INTO `san_pham` (`ma_san_pham`, `ten_san_pham`, `gia`, `gia_khuyen_mai`, `anh`, `so_luong`, `mo_ta`, `ma_nha_san_xuat`, `ma_loai_san_pham`) VALUES
(4, 'Bộ phấn tiên MASTERS', 112, 83, 'phan_tien.jpg', 5, '- Phấn Maters Pastel không chứa hóa chất độc hại.\r\n- Các màu có thể hòa trộn nhiều lớp để tạo thành những màu khác.\r\n- Chất phấn mịn, sáng, lên màu chuẩn, không bị rơi bụi quá nhiều khi vẽ.\r\n- Phấn có thành phần màu nhuộm nên có thể dùng nhuộm tóc tạm thời. Dễ dàng gội rửa đi bằng nước hoặc dầu gội thông thường.', 'TQ', 4),
(5, 'Bút Marker MARVY đen ', 30, 30, 'bút maker.jpg', 12, 'Bút 2 đầu, 1 đầu cọ và 1 đầu nét\r\nĐầu bút mềm, màu tươi, gốc màu nước\r\nCó thể chuyển màu từ đậm sang nhạt bằng cách chấm vào nước', 'NB', 5),
(6, 'Bút Marker MARVY ', 312, 290, 'butcomau.jpg', 17, 'Tập đoàn Uchida được thành lập từ hơn 40 năm, chuyên về các sản phẩm văn phòng, đồ thủ công và nghệ thuật có chất lượng cao. Tiêu chí sản phẩm tập trung đến mọi đối tượng : công ty kinh doanh, người làm thủ công sáng tạo, cho đến người có sở thích trong lĩnh vực nghệ thuật từ chuyên nghiệp đến không nghiệp.\r\n- Trọn bộ Le Plume Permanent marker gồm 144 màu (gồm 141 màu thường và 3 màu trắng - vàng - bạc)\r\n- Bút marker MARVY có gam màu tươi sáng, thiết kế ngòi Brush rất mềm thích hợp với mọi bề mặt giấy, tạo độ mịn tối đa cho tranh vẽ, có thể kết hợp với các loại bút marker khác.\r\n- Mực gốc cồn nên khô nhanh, lúc mực khô không bị lem khi gặp nước, khó phai màu.', 'TQ', 6),
(7, 'Mực vẽ WINSOR & NEWTON', 75, 56, 'mucve.jpg', 6, '- Thiết kế lọ thủy tinh theo phong cách cổ điển\r\n- Mực khô nhanh, không thấm nước sau khi khô \r\n- Mực gốc nước, màu sắc trong, tươi sáng, được tạo ra theo công thức mực in truyền thống của Trung Hoa\r\n- Có thể pha các màu với nhau hoặc tô chồng lớp để tạo hiệu ứng riêng biệt\r\n- Dùng được với cọ, bút sắt, hoặc bút phun sơn mỹ thuật', 'P', 7),
(8, 'Bộ bút chì phác thảo MARCO', 110, 83, 'butchiphacthao.jpg', 17, '- MARCO là thương hiệu bút chì được biết đến trên toàn cầu, thuộc sở hữu của Công ty Axus (Thượng Hải), một trong những nhà sản xuất bút chì gỗ lớn nhất thế giới. Từ khi thành lập vào năm 1992, Marco luôn theo tiêu chí “Chất lượng - An toàn - Bền vững” trong suốt hơn 25 năm qua. Bắt đầu từ năm 2009, Marco là thương hiệu đầu tiên áp dụng khái niệm “Không độc hại” vào sản phẩm của mình và được biết đến như là “Thương hiệu của Thượng Hải”. Các sản phẩm của Marco được người tiêu dùng tin cậy và yêu mến ở hơn 80 quốc gia trong hơn 25 năm.\r\n- Bút chì phác thảo MARCO RENOIR có thân chì làm từ gỗ tự nhiên và không độc hại, thiết kế thân bút bằng gỗ cứng cáp, giúp hạn chế gãy ngòi khi vẽ.\r\n- Hộp có thiết kế hộp thiếc tinh tế, giúp bảo quản chì tốt nhất.\r\n- Chất chì mềm mịn, đều màu, có nhiếu cấp độ từ nhạt sang đậm, dễ dàng điều chỉnh theo ý muốn.\r\n- Được sử dụng bởi các họa sĩ chuyên nghiệp, kiến trúc sư, nhà thiết kế thời trang.', 'TQ', 8),
(9, 'Bút đi nét màu MICRON', 33, 26, 'butnet.jpg', 5, 'SAKURA được thành lập vào năm 1921, với khởi đầu là một công ty bút chì nhưng chưa được nhiều người biết đến, cho đến đầu thập niên 80, Sakura thành công với phát minh công nghệ đột phá bởi mực gel đầu tiên trên thế giới và được Văn Phòng Bằng Sáng Chế của Nhật Bản trao giải thưởng Nhà sáng chế uy tín vào năm 2000. \r\nTiếp tục thành công đó, Sakura đã phát triển thêm các loại màu vẽ khác có giá cả phù hợp cho người tiêu dùng ở mọi lứa tuổi.\r\n- Bút đi nét MICRON SAKURA đầu PN có mực khô nhanh và không bị lem khi gặp nước nên rất hợp với tranh màu nước, công thức mực chống phai nên khả năng bền màu rất cao. \r\n- Đầu bút bắng nhựa, nét bút mượt, đều màu, có thể tạo ra nhiều nét vẽ đa dạng, đặc biệt hạn chế mực thấm qua mặt giấy sau cho dù là giấy mỏng.\r\n- Nếu vẽ kết với màu nước hoặc bút marker thì nên dùng sau khi màu đã khô rồi mới đi nét sau.', 'NB', 9),
(10, 'Giấy vẽ Canson Ý FABRIANO', 66, 17, 'giayve.jpg', 11, 'Gân giấy nhỏ, mịn, loang màu tốt, thuộc dòng Smooth\r\nDùng được cho Màu Nước, Màu Bột, Màu Acrylic\r\nRất dễ sử dụng, có thể chỉnh sửa, thậm chí rửa được trong nước', 'P', 10),
(12, 'Bảng viết điện tử thông minh', 415, 350, 'bangvedt.jpg', 17, 'Bảng viết điện tử thông minh không cần xóa đa năng là sản phẩm có thể giúp bé học viết, vẽ hoặc có thể dùng cho sinh viên hoặc những nhân viên văn phòng viết báo cáo, vẽ đồ họa,……VSON là thương hiệu nổi tiếng của trung quốc chuyên về các dòng bút và bảng điện từ được khẳng đinh lâu đời và chất lượng.Ở Việt Nam mọi người đã tin dùng các sản phẩm bút trình chiếu VSON rất lâu và hiện nay là các dòng bảng điện tử.', 'TQ', 12),
(13, 'CỌ LÔNG SÓC NEVSKAYA', 115, 75, 'cove.jpg', 11, 'No', 'TH', 13),
(20, 'Sổ', 25, NULL, '1547137886.jpg', 0, '', 'DL', 11),
(26, 'mực vẽ', 56, NULL, '1547409881.jpg', 0, '', 'NB', 7);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`ma_admin`),
  ADD UNIQUE KEY `email_admin` (`email_admin`);

--
-- Chỉ mục cho bảng `danh_muc`
--
ALTER TABLE `danh_muc`
  ADD PRIMARY KEY (`ma_danh_muc`),
  ADD UNIQUE KEY `ten_danh_muc` (`ten_danh_muc`);

--
-- Chỉ mục cho bảng `danh_muc_san_pham`
--
ALTER TABLE `danh_muc_san_pham`
  ADD PRIMARY KEY (`ma_san_pham`,`ma_danh_muc`),
  ADD KEY `ma_danh_muc` (`ma_danh_muc`);

--
-- Chỉ mục cho bảng `hoa_don`
--
ALTER TABLE `hoa_don`
  ADD PRIMARY KEY (`ma_hoa_don`),
  ADD KEY `ma_khach_hang` (`ma_khach_hang`);

--
-- Chỉ mục cho bảng `hoa_don_chi_tiet`
--
ALTER TABLE `hoa_don_chi_tiet`
  ADD PRIMARY KEY (`ma_hoa_don`,`ma_san_pham`),
  ADD KEY `ma_san_pham` (`ma_san_pham`);

--
-- Chỉ mục cho bảng `khach_hang`
--
ALTER TABLE `khach_hang`
  ADD PRIMARY KEY (`ma_khach_hang`),
  ADD UNIQUE KEY `email_khach_hang` (`email_khach_hang`);

--
-- Chỉ mục cho bảng `loai_san_pham`
--
ALTER TABLE `loai_san_pham`
  ADD PRIMARY KEY (`ma_loai_san_pham`),
  ADD UNIQUE KEY `ten_loai_san_pham` (`ten_loai_san_pham`);

--
-- Chỉ mục cho bảng `nha_san_xuat`
--
ALTER TABLE `nha_san_xuat`
  ADD PRIMARY KEY (`ma_nha_san_xuat`);

--
-- Chỉ mục cho bảng `san_pham`
--
ALTER TABLE `san_pham`
  ADD PRIMARY KEY (`ma_san_pham`),
  ADD UNIQUE KEY `ten_san_pham` (`ten_san_pham`),
  ADD KEY `ma_nha_san_xuat` (`ma_nha_san_xuat`),
  ADD KEY `ma_loai_san_pham` (`ma_loai_san_pham`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `admin`
--
ALTER TABLE `admin`
  MODIFY `ma_admin` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT cho bảng `danh_muc`
--
ALTER TABLE `danh_muc`
  MODIFY `ma_danh_muc` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT cho bảng `hoa_don`
--
ALTER TABLE `hoa_don`
  MODIFY `ma_hoa_don` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `khach_hang`
--
ALTER TABLE `khach_hang`
  MODIFY `ma_khach_hang` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `loai_san_pham`
--
ALTER TABLE `loai_san_pham`
  MODIFY `ma_loai_san_pham` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT cho bảng `san_pham`
--
ALTER TABLE `san_pham`
  MODIFY `ma_san_pham` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `danh_muc_san_pham`
--
ALTER TABLE `danh_muc_san_pham`
  ADD CONSTRAINT `danh_muc_san_pham_ibfk_1` FOREIGN KEY (`ma_san_pham`) REFERENCES `san_pham` (`ma_san_pham`),
  ADD CONSTRAINT `danh_muc_san_pham_ibfk_2` FOREIGN KEY (`ma_danh_muc`) REFERENCES `danh_muc` (`ma_danh_muc`);

--
-- Các ràng buộc cho bảng `hoa_don`
--
ALTER TABLE `hoa_don`
  ADD CONSTRAINT `hoa_don_ibfk_1` FOREIGN KEY (`ma_khach_hang`) REFERENCES `khach_hang` (`ma_khach_hang`);

--
-- Các ràng buộc cho bảng `hoa_don_chi_tiet`
--
ALTER TABLE `hoa_don_chi_tiet`
  ADD CONSTRAINT `hoa_don_chi_tiet_ibfk_1` FOREIGN KEY (`ma_hoa_don`) REFERENCES `hoa_don` (`ma_hoa_don`),
  ADD CONSTRAINT `hoa_don_chi_tiet_ibfk_2` FOREIGN KEY (`ma_san_pham`) REFERENCES `san_pham` (`ma_san_pham`);

--
-- Các ràng buộc cho bảng `san_pham`
--
ALTER TABLE `san_pham`
  ADD CONSTRAINT `san_pham_ibfk_1` FOREIGN KEY (`ma_nha_san_xuat`) REFERENCES `nha_san_xuat` (`ma_nha_san_xuat`),
  ADD CONSTRAINT `san_pham_ibfk_2` FOREIGN KEY (`ma_loai_san_pham`) REFERENCES `loai_san_pham` (`ma_loai_san_pham`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
