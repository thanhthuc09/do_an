<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Hóa Đơn Đã Duyệt</title>
</head>
<body>
	<h1>Hóa Đơn Đã Duyệt</h1>
	<?php
		// $file_header_admin = "../result_admin.php";
		// require ('../form_login_admin.php');
		require_once('../kiem_tra_admin.php');
		require_once('../../ket_noi.php');
		$query = "SELECT * FROM `hoa_don`
				JOIN khach_hang 
				ON hoa_don.ma_khach_hang=khach_hang.ma_khach_hang
				JOIN hoa_don_chi_tiet
				ON hoa_don.ma_hoa_don = hoa_don_chi_tiet.ma_hoa_don
				JOIN san_pham
				ON hoa_don_chi_tiet.ma_san_pham = san_pham.ma_san_pham
				WHERE tinh_trang = 2
				ORDER BY thoi_gian_dat_hang asc";
		$result = mysqli_query($connect,$query);//print_r($query);die();
		$array = array();
		while ($row = mysqli_fetch_array($result))  {
			$ma_hoa_don = $row['ma_hoa_don'];
			$array[$ma_hoa_don]['ten_khach_hang']			= $row['ten_khach_hang'];
			$array[$ma_hoa_don]['sdt_khach_hang']	= $row['sdt_khach_hang'];
			$array[$ma_hoa_don]['dia_chi_khach_hang']					= $row['dia_chi_khach_hang'];
			$array[$ma_hoa_don]['thoi_gian_dat_hang'] = $row['thoi_gian_dat_hang'];

			$ma_san_pham = $row['ma_san_pham'];
			$array[$ma_hoa_don]['san_pham'][$ma_san_pham]['ten_san_pham']= $row['ten_san_pham'];
			$array[$ma_hoa_don]['san_pham'][$ma_san_pham]['gia']= $row['gia'];
			$array[$ma_hoa_don]['san_pham'][$ma_san_pham]['so_luong']= $row['so_luong'];
		}
	?>
	<table width="100%" border="1">
		<tr>
			<td>Tên khách hàng</td>
			<td>Thời gian mua</td>
			<td>Sản phẩm</td>
		</tr>
		<?php 
			foreach ($array as $ma_hoa_don => $tung_hoa_don) {
			$tong_tien = 0;
		?>
		<tr>
			<td>
				<?php 
					$ten_khach_hang = $tung_hoa_don['ten_khach_hang'];
					echo "<b>Tên:</b>"." ".$ten_khach_hang;
					echo "<br>";
					echo "<b>Số điện thoại:</b>"." ".$tung_hoa_don['sdt_khach_hang'];
					echo "<br>";
					echo "<b>Địa chỉ:</b>"." ".$tung_hoa_don['dia_chi_khach_hang'];
				?>
			</td>
			<td>
				<?php 
					$thoi_gian_mua = $tung_hoa_don['thoi_gian_dat_hang'];
					$thoi_gian_da_sua   = date("d-m-Y H:i", strtotime($thoi_gian_mua));
					echo $thoi_gian_da_sua;
				?>
			</td>
			<td>
				<ul>
					<?php 
					foreach ($tung_hoa_don['san_pham'] as $tung_san_pham) {
					?>
					<li>
						<?php echo $tung_san_pham['ten_san_pham'] ?>	
					</li>
					<?php
						$tong_tien = $tong_tien + ($tung_san_pham['so_luong'] * $tung_san_pham['gia']);
						} 
					?>
					<b>Tiền đã được: <?php echo $tong_tien; ?></b>
				</ul>
			</td>
		</tr>
		<?php  
			}
		?>
	</table>
</body>
</html>