<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<?php
		$file_header_admin = "../dang_nhap_form.php"; 
		require_once('../kiem_tra_admin.php');
		require_once('../../ket_noi.php');
		$query = "select * from hoa_don
		join khach_hang
		on hoa_don.ma_khach_hang = khach_hang.ma_khach_hang
		where tinh_trang = 1
		order by thoi_gian_dat_hang";
		$result = mysqli_query($connect,$query);
		mysqli_close($connect);
	?>
	<table width="100%" border="1">
		<tr>
			<th>Ngày đặt hàng</th>
			<th>Thời gian đặt hàng</th>
			<th>Khách Hàng</th>
			<th>Xem Chi Tiết</th>
			<th>Duyệt</th>
			<th>Hủy</th>
		</tr>
		<?php while($row = mysqli_fetch_array($result)){ ?>
			<tr>
				<td>
					<?php 
						$thoi_gian_dat_hang = $row['thoi_gian_dat_hang'];
						$thoi_gian_da_sua   = date("d-m", strtotime($thoi_gian_dat_hang));
						echo $thoi_gian_da_sua; 
					?>
				</td>
				<td>
					<?php 
						$thoi_gian_da_sua   = date("H:i", strtotime($thoi_gian_dat_hang));
						echo $thoi_gian_da_sua; 
					?>
				</td>
				<td>
					<?php echo $row['ten_khach_hang'] ?>
					<br>
					SDT:<?php echo $row['sdt_khach_hang'] ?>
					<br>
					Địa chỉ:<?php echo $row['dia_chi_khach_hang'] ?>
				</td>
				<td>
					<a href="hoa_don_chi_tiet_view.php?ma_hoa_don=<?php echo $row['ma_hoa_don'] ?>">
						Xem Chi Tiết
					</a>
				</td>
				<td>
					<a href="thay_doi_tinh_trang_hoa_don.php?ma_hoa_don=<?php echo $row['ma_hoa_don'] ?>&kieu=duyet">
						Duyệt
					</a>
				</td>
				<td>
					<a href="thay_doi_tinh_trang_hoa_don.php?ma_hoa_don=<?php echo $row['ma_hoa_don'] ?>&kieu=huy">
						Hủy
					</a>
				</td>
			</tr>
		<?php } ?>
	</table>
</body>
</html>