<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="../css/style.css">
</head>
<body>
	<?php 
		require_once('../kiem_tra_admin.php');
		require_once('../../ket_noi.php');
		//lấy toàn bộ nhà sản xuất để chọn
		$query_nxs = "select * from nha_san_xuat";
		$result_nxs = mysqli_query($connect,$query_nxs);

		//lấy thông tin sản phẩm muốn sửa
		$ma_san_pham = $_GET['ma_san_pham'];
		$query_sp = "select * from san_pham 
		where ma_san_pham = '$ma_san_pham'";
		$result_sp = mysqli_query($connect,$query_sp);
		$row_sp = mysqli_fetch_array($result_sp);
		mysqli_close($connect);
	?>
	<form action="san_pham_update_process.php" method="post" enctype="multipart/form-data">
		<input type="hidden" name="ma_san_pham" value="<?php echo $row_sp['ma_san_pham'] ?>">
		Tên Sản Phẩm
		<input type="text" name="ten_san_pham" value="<?php echo $row_sp['ten_san_pham'] ?>">
		<br>
		Giá
		<input type="number" name="gia" value="<?php echo $row_sp['gia'] ?>">
		<br>
		Ảnh cũ
		<img src="anh/<?php echo $row_sp['anh'] ?>">
		<input type="hidden" name="anh_cu" value="<?php echo $row_sp['anh'] ?>">
		<br>
		Chọn ảnh mới
		<input type="file" name="anh_moi" accept="image/*">
		<br>
		Nhà sản xuất
		<select name="ma_nha_san_xuat">
			<?php 
				while ($row_nxs = mysqli_fetch_array($result_nxs)) {
			?>
			<option value="<?php echo $row_nxs['ma_nha_san_xuat'] ?>"
			<?php 
				if($row_nxs['ma_nha_san_xuat']==$row_sp['ma_nha_san_xuat']){
					echo "selected";
				}
			?>
			>
				<?php echo $row_nxs['ten_nha_san_xuat'] ?>
			</option>
			<?php 
				}
			?>
		</select>
		<br>
		<button name="button_submit" value="1">Sửa sản phẩm</button>
	</form>
</body>
</html>