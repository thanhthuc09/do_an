<!DOCTYPE html>
<html>
<head>
	<title>Trang admin</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="../css/menu.css">
	<link rel="stylesheet" type="text/css" href="../css/style.css">
	<style type="text/css">
		@import url("https://fonts.googleapis.com/css?family=Poppins");
		.card {
		  border-radius: 16px;
		  width: 33%;
		  height: 360px;
		  position: relative;
		  overflow: hidden;
		  box-shadow: 0 5px 50px rgba(0, 0, 0, 0.85);
		  float: left;
		}
		.card:before {
		  content: "";
		  position: absolute;
		  width: 100%;
		  height: 100%;
		  top: 0;
		}
		.card .img {
		  position: absolute;
		  top: 0;
		  left: 0;
		  width: 100%;
		  height: 100%;
		  display: flex;
		}
		.card .img span {
		  width: 100%;
		  height: 100%;
		  transition: 0.5s;
		  background-size: cover;
		}

		.card:hover .img > span {
		  transform: translateY(-100%);
		}
		.card:hover .content {
		  transform: translateY(0%);
		  transition: 1s;
		  transition-delay: 0.1s;
		}

		.content {
		  box-sizing: border-box;
		  display: flex;
		  justify-content: center;
		  align-items: center;
		  flex-direction: column;
		  font-weight: bold;
		  padding: 20px 20px;
		  width: 100%;
		  height: 100%;
		  transform: translateY(100%);
		  position: relative;
		}
		.content-bg {
		  content: "";
		  opacity: 0.5;
		  top: 0;
		  left: 0;
		  bottom: 0;
		  right: 0;
		  position: absolute;
		  z-index: -1;   
		}
		#div_content{
			width: 100%;
			height: 70%;
			clear: both;
		}
		#div_menu{
			width: 100%;
			height: 20%;
		}
		#div_all{
			width: 100%;
			height: 1000px;
		}
	</style>
</head>
<body>
	<div id="div_all">
		<?php
			require_once('../kiem_tra_admin.php');
			require_once('../menu.php');
			require_once('../../ket_noi.php');
			$tim_kiem = "";
			if(isset($_GET['tim_kiem'])){
				$tim_kiem = $_GET['tim_kiem'];
			}
			$query = "select * from san_pham
			join nha_san_xuat
			on san_pham.ma_nha_san_xuat = nha_san_xuat.ma_nha_san_xuat
			where ten_san_pham like '%$tim_kiem%'";
			$result = mysqli_query($connect,$query);
			$count = mysqli_num_rows($result);
			mysqli_close($connect);
		?>
		<?php echo "<h2 align='center'>Có $count sản phẩm</h2>" ?>
		<div id="div_content">
			<?php
				while($row = mysqli_fetch_array($result)){
			?>
			<div class="card">
			  <div class="img"> 
			  	<span style="background-image: url('anh/<?php echo $row['anh'] ?>');"></span>
			  </div>
			  <div class="content">
			    <h2><?php echo $row['ten_san_pham'] ?></h2>
			    <h2><?php echo $row['gia'] ?> VNĐ</h2>
			    <h4><a href="#">Thêm vào giỏ hàng</a></h4>
			  </div>
			  <div class="content-bg" style="background-image: url('anh/<?php echo $row['anh'] ?>');background-size: cover">
			  </div>
			</div>		
			<?php
				}
			?>
			<a href="san_pham_insert_form.php">Thêm</a>
		</div>
	</div>
</body>
</html>