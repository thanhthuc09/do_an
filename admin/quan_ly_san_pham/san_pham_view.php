<!DOCTYPE html>
<html>
<head>
	<title>Trang admin</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="../css/menu.css">
	<link rel="stylesheet" type="text/css" href="../css/style.css">
</head>
<body>
	<?php
		require_once('../kiem_tra_admin.php');
		require_once('menu.php');
		require_once('../../ket_noi.php');
		$tim_kiem = "";
		if(isset($_GET['tim_kiem'])){
			$tim_kiem = $_GET['tim_kiem'];
		}
		$page = 1;
		if(isset($_GET['page'])){
			$page = $_GET['page'];
		}
		//giới hạn sản phẩm của 1 trang
		$limit = 3;

		//bỏ qua bao nhiêu sản phẩm
		$offset = ($page-1)*$limit;

		//lấy sản phẩm để hiển thị trên 1 trang
		$query_show = "select * from san_pham
		join nha_san_xuat
		on san_pham.ma_nha_san_xuat = nha_san_xuat.ma_nha_san_xuat
		where ten_san_pham like '%$tim_kiem%'
		limit $limit offset $offset";
		$result_show = mysqli_query($connect,$query_show);

		$query_count = "select count(*) from san_pham
		where ten_san_pham like '%$tim_kiem%'";
		$result_count = mysqli_query($connect,$query_count);
		$row_count    = mysqli_fetch_array($result_count);
		$count        = $row_count['count(*)'];

		$total_page = ceil($count/$limit);
		mysqli_close($connect);
	?>
	<?php echo "<h2 align='center'>Có $count sản phẩm</h2>" ?>
	<div id="content">
		<table border="1" width="100%">
			<caption>
				<form>
					Tìm kiếm: 
					<input type="text" name="tim_kiem" value="<?php echo $tim_kiem ?>">
					<button>Tìm</button>
				</form>
			</caption>
			<tr>
				<th>#</th>
				<th>Tên</th>
				<th>Giá</th>
				<th>Ảnh</th>
				<th>Nhà Sản Xuất</th>
				<th>Sửa</th>
				<th>Xóa</th>
			</tr>
			<?php
				while($row = mysqli_fetch_array($result_show)){
			?>
			<tr>
				<td><?php echo $row['ma_san_pham'] ?></td>
				<td><?php echo $row['ten_san_pham'] ?></td>
				<td><?php echo $row['gia'] ?></td>
				<td><img src="anh/<?php echo $row['anh'] ?>"></td>
				<td><?php echo $row['ten_nha_san_xuat'] ?></td>
				<td>
					<a href='san_pham_update_form.php?ma_san_pham=<?php echo $row['ma_san_pham'] ?>'>Sửa
					</a>
				</td>
				<td>
					<a href='san_pham_delete.php?ma_san_pham=<?php echo $row['ma_san_pham'] ?>&anh=<?php echo $row['anh'] ?>'>Xóa
					</a>
				</td>
			</tr>
			<?php
				}
			?>
		</table>
		<a href="san_pham_insert_form.php">Thêm</a>
		<div align="center">
			<?php for($i=1; $i<=$total_page; $i++){ ?>
				<a href="?page=<?php echo $i ?>&tim_kiem=<?php echo $tim_kiem ?>"><?php echo $i ?></a>
			<?php } ?>
		</div>
	</div>
</body>
</html>