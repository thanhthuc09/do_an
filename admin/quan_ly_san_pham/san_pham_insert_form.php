<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<?php 
		require_once('../kiem_tra_admin.php');
		require_once('../../ket_noi.php');
		$query_nsx = "select * from nha_san_xuat";
		$result_nsx = mysqli_query($connect,$query_nsx);
		$query_lsp = "select * from loai_san_pham";
		$result_lsp = mysqli_query($connect,$query_lsp);
	?>
	<form action="san_pham_insert_process.php" method="post" enctype="multipart/form-data">
		Tên Sản Phẩm
		<input type="text" name="ten_san_pham">
		<br>
		Giá
		<input type="number" name="gia">
		<br>
		Ảnh
		<input type="file" name="anh" accept="image/*">
		<br>
		Nhà sản xuất
		<select name="ma_nha_san_xuat">
			<?php 
				while ($row = mysqli_fetch_array($result_nsx)) {
			?>
				<option value="<?php echo $row['ma_nha_san_xuat'] ?>">
					<?php echo $row['ten_nha_san_xuat'] ?>
				</option>
			<?php 
				}
			?>
		</select>
		<br>
		Loại sản phẩm
		<select name="ma_loai_san_pham">
			<?php 
				while ($row = mysqli_fetch_array($result_lsp)) {
			?>
			<option value="<?php echo $row['ma_loai_san_pham'] ?>">
				<?php echo $row['ten_loai_san_pham'] ?>
			</option>
			<?php 
				}
			?>
		</select>
		<br>
		<button name="button_submit" value="1">Thêm sản phẩm</button>
	</form>
</body>
</html>