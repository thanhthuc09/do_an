<?php session_start(); ?>
<!DOCTYPE html>
<html>
<head>
	<title>Tất cả sản phẩm</title>
	<meta charset="utf-8">
	<style type="text/css">
		
		ul{
	list-style-type: none;
	float: left;
	padding: 0;
}
ul li{
	float: left;
	width: 100px;
	background: pink;
	height: 50px;
	border: 1px solid;
	position: relative;
}
a{
	text-decoration: none;
	color: red;
}
ul li ul{
	display: none;
	position:absolute;
	top: 51px;
	left: -1px; 
}
ul li:hover ul{
	display: block;
}
	</style>
</head>
<body>
<div id="div_menu">
	<ul>
		<li>
			<a href="#">Quản lý sản phẩm</a>
			<ul>
				<li>
					<a href="quan_ly_san_pham/san_pham_view.php">Xem sản phẩm</a>
				</li>
				<li>
					<a href="quan_ly_san_pham/san_pham_insert_form.php">Thêm sản phẩm</a>
				</li>
			</ul>
		</li>
		<li>
			<a href="#">Quản lý hóa đơn</a>
			<ul>
				<li>
					<a href="quan_ly_hoa_don/hoa_don_chua_duyet.php">Xem hóa đơn chưa duyệt</a>
				</li>
				<li>
					<a href="quan_ly_hoa_don/hoa_don_da_duyet.php">Xem hóa đơn đã duyệt</a>
				</li>
				<li>
					<a href="quan_ly_hoa_don/hoa_don_da_huy.php">Xem hóa đơn đã hủy</a>
				</li>
			</ul>
		</li>
		<?php if($_SESSION['cap_do']==1){ ?>
		<li>
			<a href="#">Quản lý nhân viên</a>
			<ul>
				<li>
					<a href="quan_ly_nhan_vien/nhan_vien_view.php">Xem nhân viên</a>
				</li>
				<li>
					<a href="quan_ly_nhan_vien/insert_form_nhan_vien.php">Thêm nhân viên</a>
				</li>
			</ul>
		</li>
		
		<?php } ?>
		<li>
			<a href="update_mat_khau.php">Đổi mật khẩu</a>
		</li>
		<li>
			<a href="dang_xuat.php">Đăng xuất</a>
		</li>
		<li>
			<a href="#"><?php echo $_SESSION['ten_admin'] ?></a>
		</li>
	</ul>
</div>
</body>