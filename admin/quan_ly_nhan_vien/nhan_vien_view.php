<!DOCTYPE html>
<html>
<head>
	<title>Trang admin</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="../css/menu.css">
	<link rel="stylesheet" type="text/css" href="../css/style.css">
</head>
<body>
	<?php 
		require_once('../kiem_tra_sadmin.php');
		//require_once('../menu.php');
		require_once('../../ket_noi.php');
		$query = 'select * from admin'; 
		$result = mysqli_query($connect,$query);
		//die($query);
	?>
	<table border="1" width="100%">
		<tr>
			<th>Mã </th>
			<th>Tên </th>
			<th>Email</th>
			<th>Địa chỉ</th>
			<th>SĐT</th>
			<th>Ngày sinh</th>
			<th>Giới tính</th>
			<th>Cấp độ</th>
			<th>Sửa</th>
			<th>Xóa</th>
		</tr>
		<?php
			while ($row = mysqli_fetch_array($result)) {
		?>
			<tr>
				<td><?php echo $row['ma_admin'] ?></td>
				<td><?php echo $row['ten_admin'] ?></td>
				<td><?php echo $row['email_admin'] ?></td>
				<td><?php echo $row['dia_chi_admin'] ?></td>
				<td><?php echo $row['sdt_admin'] ?></td>
				<td><?php echo $row['ngay_sinh_admin'] ?></td>
				<td>
					<?php
						if($row['gioi_tinh_admin']==1){
					 		echo "Nam";
						}
						else{
							echo "Nữ";
						}
					?>
				</td>
				<td>
					<?php
						if($row['cap_do']==1){
					 		echo "Super Admin";
						}
						else{
							echo "Admin";
						}
					?>
				</td>
				<td>
					<a href='update_form_nhan_vien.php?ma_admin=<?php echo $row['ma_admin'] ?>'>Sửa
					</a>
				</td>
				<td>
					<a href='delete_nhan_vien.php?ma_admin=<?php echo $row['ma_admin'] ?>'>Xóa
					</a>
				</td>
		<?php
			}
		?>
	</table>
	<a href="insert_form_nhan_vien.php"><button>Thêm nhân viên</button></a>
</body>
</html>