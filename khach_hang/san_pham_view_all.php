<!DOCTYPE html>
<html>
<head>
	<title>Tất cả sản phẩm</title>
	<meta charset="utf-8">
	<style type="text/css">
		@import url("https://fonts.googleapis.com/css?family=Poppins");
		.card {
		  border-radius: 16px;
		  width: 30%;
		  height: 360px;
		  position: relative;
		  overflow: hidden;
		  box-shadow: 0 5px 50px rgba(0, 0, 0, 0.85);
		  float: left;
		  margin: 1%;
		}
		.card:before {
		  content: "";
		  position: absolute;
		  width: 100%;
		  height: 100%;
		  top: 0;
		}
		.card .img {
		  position: absolute;
		  top: 0;
		  left: 0;
		  width: 100%;
		  height: 100%;
		  display: flex;
		}
		.card .img span {
		  width: 100%;
		  height: 100%;
		  transition: 0.5s;
		  background-size: cover;
		}

		.card:hover .img > span {
		  transform: translateY(-100%);
		}
		.card:hover .content {
		  transform: translateY(0%);
		  transition: 1s;
		  transition-delay: 0.1s;
		}

		.content {
		  box-sizing: border-box;
		  display: flex;
		  justify-content: center;
		  align-items: center;
		  flex-direction: column;
		  font-weight: bold;
		  padding: 20px 20px;
		  width: 100%;
		  height: 80%;
		  transform: translateY(100%);
		  position: relative;
		}
		.content-bg {
		  content: "";
		  opacity: 0.5;
		  top: 0;
		  left: 0;
		  bottom: 0;
		  right: 0;
		  position: absolute;
		  z-index: -1;   
		}
		#div_content{
			width: 100%;
			height: 100%;
			clear: both;
		}
		#div_menu{
			width: 100%;
			height: 20%;
		}
		#div_all{
			width: 100%;
			height: 400px;
		}
		.gach_ngang{
			text-decoration: line-through;
		}
		ul{
	list-style-type: none;
	float: left;
	padding: 0;
}
ul li{
	float: left;
	width: 100px;
	background: pink;
	height: 50px;
	border: 1px solid;
	position: relative;
}
a{
	text-decoration: none;
	color: red;
}
ul li ul{
	display: none;
	position:absolute;
	top: 51px;
	left: -1px; 
}
ul li:hover ul{
	display: block;
}
	</style>
</head>
<body>
<div id="menu">
			<ul >
				<li><a href="../index.php">Trang chủ</a></li>
				
				<li ><a href="chinh_sua_thong_tin_view.php" >Chỉnh sửa thông tin</a></li>
				<li ><a href="doi_mat_khau_view.php">Đổi mật khẩu</a></li>
				<li><a href="xem_gio_hang.php">Giỏ hàng</a></li>
				
				<li>
					<a href="dang_xuat_khach_hang.php">Đăng xuất</a>
				</li>
				
			</ul>
</div>
	<div id="div_all">

	<?php 
		require_once('../ket_noi.php');
		$tim_kiem = "";
		if(isset($_GET['tim_kiem'])){
			$tim_kiem = $_GET['tim_kiem'];
		}

		//vào trang đầu tiên
		$trang = 1;
		if(isset($_GET['trang'])){
			$trang = $_GET['trang'];
		}
		//số sp hiển thị trên 1 trang
		$limit = 3;

		//số sản phẩm bỏ qua
		$offset = ($trang-1)*$limit;


		//câu lệnh lấy sản phẩm trong 1 trang
		$query = "select * from san_pham
		join nha_san_xuat
		on san_pham.ma_nha_san_xuat = nha_san_xuat.ma_nha_san_xuat
		where ten_san_pham like '%$tim_kiem%'
		limit $limit offset $offset";
		$result = mysqli_query($connect,$query);

		//lấy tổng số sản phẩm
		$query_count  = "select count(*) from san_pham
		where ten_san_pham like '%$tim_kiem%'";
		$result_count = mysqli_query($connect,$query_count);
		$row_count    = mysqli_fetch_array($result_count);
		$count        = $row_count['count(*)'];

		//số trang
		$so_trang = ceil($count/$limit);

		mysqli_close($connect);
	?>
		<div align="center">
			<form>
				Tìm kiếm: 
				<input type="text" name="tim_kiem" value="<?php echo $tim_kiem ?>">
				<button>Tìm</button>
			</form>
			<h1>Tổng số sản phẩm <?php echo $count ?></h1>
		</div>
		<div id="div_content">
			<?php
				while($row = mysqli_fetch_array($result)){
			?>
			<div class="card">
			  <div class="img"> 
			  	<span style="background-image: url('../admin/quan_ly_san_pham/anh/<?php echo $row['anh'] ?>');"></span>
			  </div>
			  <div class="content">
			    <h2><?php echo $row['ten_san_pham'] ?></h2>
			    <h2>Giá gốc <span class="gach_ngang"><?php echo ($row['gia']+rand(1,10)) ?> VNĐ</span></h2>
			    <h2>Giá đã giảm <?php echo $row['gia'] ?> VNĐ</h2>
			    <h3>NSX:<?php echo $row['ten_nha_san_xuat'] ?></h3>
			    <h3>
			    	<a href="them_vao_gio_hang.php?ma_san_pham=<?php echo $row['ma_san_pham'] ?>">
			    		Thêm vào giỏ hàng
			    	</a>
			    </h3>
			    <!-- <h3>
			    	<a href="san_pham_view_one.php?ma_san_pham=<?php echo $row['ma_san_pham'] ?>">
				    	Xem chi tiết
				    </a>
				</h3> -->
			  </div>
			  <div class="content-bg" style="background-image: url('../admin/quan_ly_san_pham/anh/<?php echo $row['anh'] ?>');background-size: cover">
			  </div>
			</div>		
			<?php
				}
			?>
		</div>
		<div align="center">
			<?php for($i=1; $i<=$so_trang; $i++){ ?>
				<a href="?trang=<?php echo $i ?>&tim_kiem=<?php echo $tim_kiem ?>"><?php echo $i ?></a>
			<?php } ?>
			<h1><a href="xem_gio_hang.php">Xem Giỏ Hàng</a></h1>
		</div>
	</div>
</body>
</html>