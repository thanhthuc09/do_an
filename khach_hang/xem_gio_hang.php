<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<?php 
		require_once('kiem_tra_khach_hang.php');
		if(!empty($_SESSION['gio_hang'])){
			$ma_khach_hang = $_SESSION['ma_khach_hang'];
			require_once('../ket_noi.php');
			$query  = "select * from khach_hang where ma_khach_hang = '$ma_khach_hang'";
			$result = mysqli_query($connect,$query);
			$row = mysqli_fetch_array($result);
	?>
	<table border="1" width="100%">
		<tr>
			<th>Tên Sản Phẩm</th>
			<th>Ảnh</th>
			<th>Giá</th>
			<th>Số Lượng</th>
			<th>Xóa</th>
		</tr>
		<?php 
			$tong = 0;
			foreach ($_SESSION['gio_hang'] as $ma_san_pham => $tung_san_pham) { 
		?>
			<tr>
				<td><?php echo $tung_san_pham['ten_san_pham'] ?></td>
				<td><?php echo $tung_san_pham['anh'] ?></td>
				<td><?php echo $tung_san_pham['gia'] ?></td>
				<td>
					<a href="thay_doi_so_luong_san_pham_trong_gio_hang.php?kieu=tru&ma_san_pham=<?php echo $ma_san_pham ?>">-</a>
					<?php echo $tung_san_pham['so_luong'] ?>
					<a href="thay_doi_so_luong_san_pham_trong_gio_hang.php?kieu=cong&ma_san_pham=<?php echo $ma_san_pham ?>">+</a>
				</td>
				<td>
					<a href="xoa_san_pham_trong_gio_hang.php?ma_san_pham=<?php echo $ma_san_pham ?>">
						Xóa
					</a>
				</td>
			</tr>
		<?php 
		$tong += $tung_san_pham['gia']*$tung_san_pham['so_luong'];
		} 
		?>
	</table>
	<h1>Tổng tiền phải tốn là: <?php echo $tong ?></h1>
	<a href="xoa_gio_hang.php">Xóa giỏ hàng</a>
	<h1>Thông tin người nhận</h1>
	<form action="dat_hang.php">
		Tên người nhận
		<input type="text" name="ten_khach_hang" value="<?php echo $row['ten_khach_hang'] ?>">
		<br>
		SDT người nhận
		<input type="text" name="sdt_khach_hang" value="<?php echo $row['sdt_khach_hang'] ?>">
		<br>
		Địa chỉ người nhận
		<textarea name="dia_chi_khach_hang"><?php echo $row['dia_chi_khach_hang'] ?></textarea>
		<br>
		<button name="button_submit" value="1">Đặt hàng</button>
	</form>
	<?php 
		}
		else{
	?>
		<h1>Không có sản phẩm nào trong giỏ hàng</h1>
	<?php } ?>
	<a href="san_pham_view_all.php">Xem tất cả sản phẩm</a>
</body>
</html>